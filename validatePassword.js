const validatePassword = (passwordString) => /^(?!\.)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\.)(?=.*[!#$%&'*+-/=?^_`{|}~])(?!.*?\.\.)(?!.*\.$).*$/.test(passwordString)

// To visualize click here: https://regexper.com/#%5E%28%3F!%5C.%29%28%3F%3D.*%5BA-Z%5D%29%28%3F%3D.*%5Ba-z%5D%29%28%3F%3D.*%5B0-9%5D%29%28%3F%3D.*%5C.%29%28%3F%3D.*%5B!%23%24%25%26'*%2B-%2F%3D%3F%5E_%60%7B%7C%7D~%5D%29%28%3F!.*%3F%5C.%5C.%29%28%3F!.*%5C.%24%29.*%24
