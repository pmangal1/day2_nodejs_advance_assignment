Array.prototype.push = function (...items) {
  for (item of items) {
    this[this.length] = item;
  }

  console.log('element added successfully');
  return this.length;
}

Array.prototype.pop = function () {
  let deleted = this[this.length - 1];
  if (this.length > 0) { // array length should be greater than 0
    this.length = this.length - 1;
    console.log('element deleted successfully');
  }

  return deleted;
}

Array.prototype.unshift = function (...items) {
  for (let j = items.length - 1; j >= 0; j--) {
    for (let i = this.length; i > 0; i--)
      this[i] = this[i - 1];
    this[0] = items[j];
  }

  console.log('element unshift successfully');
  return this.length;
}

Array.prototype.shift = function () {
  let i;
  let shifted = this[0];
  if (this.length) { // shift only if array length is greater than 0
    for (i = 0; i < this.length; i++)
      this[i] = this[i + 1];
    this.length = i - 1;
  }
  console.log('element shift successfully');
  return shifted;
}

Array.prototype.indexOf = function (search) {
  console.log('Element Searched')
  for (let i = 0; i < this.length; i++)
    if (this[i] == search) // element matched
      return i;
  return -1;
}

Array.prototype.len = function () {
  let cnt = 0;
  for (item of this)
    cnt++;
  console.log('Length Returned')
  return cnt;
}

Array.prototype.forEach = function (callback) {
  for (let i = 0; i < this.length; i++)
    callback(this[i], i)
}

Array.prototype.splice = function (pos, delcount, ...items) {
  let endArr = [], spliced = [], initialLength = this.length;
  // checking for negative indexing and delete count should be appropriate
  if (pos < 0 || pos > this.length - 1 || delcount < 0) return spliced
  delcount = (delcount || this.length - pos)

  for (let j = pos; j < pos + delcount && j < this.length; j++)
    spliced.push(this[j]);

  for (let i = pos + delcount, j = 0; i < this.length; i++, j++)
    endArr[j] = this[i]

  let i = pos;
  for (item of items) {
    this[i] = item;
    i++;
  }

  for (item of endArr) {
    this[i] = item;
    i++;
  }

  this.length = i;
  return spliced;
}