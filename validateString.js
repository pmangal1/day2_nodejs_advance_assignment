function validateString(input, callback) {
  setTimeout(function () {
    // input is said to be valid if it is a lowercase string
    if (typeof input === "string" && input === input.toLowerCase()) {
      return callback(null, true)
    }

    return callback(new Error('Invalid string'), null)
  }, 500)

}

function init(input) {
  let result = {}
  let i = 0
  function helper(err, res) {
    result[`${input[i]}`] = err ? false : true;
    i++;
    i < input.length ? validateString(input[i], helper) : console.log(result)
  }
  validateString(input[0], helper)
}

init(['first', 'Second', 'thiRd', 4, false, 'true'])