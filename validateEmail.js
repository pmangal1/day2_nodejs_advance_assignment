const validateEmail = (email) => /^[a-z_][a-z0-9_.%+-]+@[a-z0-9]+\.[a-z]{2,}$/.test(email.toLowerCase())

// To visualize click here: https://regexper.com/#%5E%5Ba-z_%5D%5Ba-z0-9_.%25%2B-%5D%2B%40%5Ba-z0-9%5D%2B%5C.%5Ba-z%5D%7B2%2C%7D%24%0A
