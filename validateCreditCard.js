const validateCreditCard = (card_number) => /^4[0-9]{12}(?:[0-9]{3})?$/.test(card_number)

// To visualize click here: https://regexper.com/#%5E4%5B0-9%5D%7B12%7D%28%3F%3A%5B0-9%5D%7B3%7D%29%3F%24
