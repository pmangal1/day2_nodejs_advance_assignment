const extractPattern = (paragraph, pattern) => paragraph.match(pattern)

const paragraph = `Lorem ipsum dolor 9221122108 sit amet, consectetur adipiscing elit, 
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Dolor sed viverra ipsum nunc aliquet bibendum enim. In massa tempor nec feugiat.
Nunc aliquet bibendum enim facilisis gravida. mytraining@deqode.com Nisl nunc mi 
ipsum faucibus vitae aliquet nec ullamcorper. Amet luctus venenatis lectus magna fringilla. 
Volutpat maecenas volutpat blandit aliquam etiam erat velit scelerisque in. 
Egestas egestas fringilla phasellus faucibus pmangal@deqode.com9131471942 scelerisque eleifend. 
+91-20200-21210 Sagittis orci a scelerisque purus semper eget duis. 
Nulla pharetra diam sit amet nisl suscipit. Sed adipiscing diam donec adipiscing 
tristique risus nec feugiat in. Fusce (+91)-20200-21210 ut placerat mt@test.inc orci nulla. 
Pharetra vel turpis nunc eget lorem dolor. Tristique senectus et netus et malesuada`

const pattern = /((\(\+\d{2}\)-)|(\+\d{2}-))?\d{5}(-?)\d{5}|[a-z_][a-z0-9_.%+-]+@[a-z0-9]+\.[a-z]{2,}/gmi
console.log(extractPattern(paragraph, pattern))

// To visualize click here: https://regexper.com/#%28%28%5C%28%5C%2B%5Cd%7B2%7D%5C%29-%29%7C%28%5C%2B%5Cd%7B2%7D-%29%29%3F%5Cd%7B5%7D%28-%3F%29%5Cd%7B5%7D%7C%5Ba-z_%5D%5Ba-z0-9_.%25%2B-%5D%2B%40%5Ba-z0-9%5D%2B%5C.%5Ba-z%5D%7B2%2C%7D
