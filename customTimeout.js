const customTimeout = (ms, callback) => {
  let currentTime = new Date().getTime();
  const endTime = new Date().getTime() + ms;
  while (endTime > currentTime) {
    currentTime = new Date().getTime();
  }

  callback();
}

customTimeout(2000, () => {
  console.log('Called After 2 Seconds')
})